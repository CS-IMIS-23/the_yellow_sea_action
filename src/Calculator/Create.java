package Calculator;

/*
Create.java         作者：赵晓海 黄宇瑭
用于生成用户指定个数指定等级的题目，且题目中含有分数
 */
public class Create {
    String[]Arraylist;//保存题目的数组
    int num,rate;//题目的个数和等级

    //构造函数
    public Create(int num ,int rate)
    {
        this.num=num;
        this.rate=rate;
        Arraylist=new String[num];
    }

    //生成相应等级的一个题目的方法
    public String questionRate(int a)
    {
        String express="";
        String[]OC={"+","-","×","÷"};

        for (int c=0;c<a;c++) {
            Fraction b = new Fraction();
            String d=b.getFraction();
            String e=OC[(int) (Math.random() * 4)];
            while (d=="0"&&e=="÷") {
                Fraction f=new Fraction();
                d = f.getFraction();

            }
            express +=d+" "+e+ " ";
        }

        Fraction c=new Fraction();
        String e=c.getFraction();
        while (express.charAt(4*rate-1)=='÷'&&e=="0")
        {
            Fraction d=new Fraction();
            e=d.getFraction();
        }

        express+=e+" "+"=";
        return express;
    }
    //生成相应个数，相应等级题目，且将其放入ArrayList数组中保存的方法
    public  void QuestionNum(){
        Create F=new Create(num,rate);
        for(int a=0;a<num;a++)
            Arraylist[a]=F.questionRate(rate);
    }

    //返回ArrayList数组的方法
    public String[] getArraylist() {
        return Arraylist;
    }

    //返回数组中指定索引处表达式的方法
    public String getArraylist2(int a)
    {
        String b;
        b=Arraylist[a];
        return b;
    }
    //输出的得到的表达式的方法
    public String toString() {
        String a="";
        for (int b=0;b<num;b++)
            a+=Arraylist[b]+"\n";
        return a;
    }
}

