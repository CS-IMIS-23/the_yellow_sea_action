package Calculator;

/*
Transform.java      作者：赵晓海  黄宇瑭
用于将一个中缀表达式转换为对应的后缀表达式
 */
import java.util.Stack;

public class Transform {
    String former;//中缀表达式

    String last;//后缀表达式

    //构造函数
    public Transform(String former){
        this.former=former;
        last="";
    }
    //得到后缀表达式的方法
    public String getLast() {
        return last;
    }

    //将中缀表达式转为后缀表达式的方法
    public void transform(){
        // int length=(former.length()+1)/2;
        //StringTokenizer toArraylist=new StringTokenizer(former);

        String []str1=former.split("\\s");

        /*int num1=0;
        while (toArraylist.hasMoreTokens()){
            str1[num1]=toArraylist.nextToken();
            num1++;
        }*/
        String [] str2=new String[str1.length-1];
        for (int a=0;a<str1.length-1;a++)
            str2[a]=str1[a];

        Stack stack=new Stack();
        int num2=0;
        while (num2<str2.length){
            if(str2[num2].equalsIgnoreCase("+")==false&&str2[num2].equalsIgnoreCase("-")==false&&str2[num2].equalsIgnoreCase("×")==false&&str2[num2].equalsIgnoreCase("÷")==false)
                last+=str2[num2]+" ";
            else
            if (stack.empty()==true)
                stack.push(str2[num2]);
            else
            if ((stack.peek().equals("+")==true||stack.peek().equals("-")==true)&&(str2[num2].equalsIgnoreCase("×")||str2[num2].equalsIgnoreCase("÷")))
                stack.push(str2[num2]);
            else {
                last+=stack.peek()+" ";
                stack.pop();
                stack.push(str2[num2]);
            }
            num2++;
        }
        while (stack.empty()==false){
            last+= stack.peek()+" ";
            stack.pop();
        }

    }
}
