package Calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

/*
Test.java             Author:赵晓海   黄宇瑭

实现生成题目、让用户答题、判断题目是否正确、统计题目个数和正确率。
 */
public class Test {
    public static void main(String[] args) {

        int num;  // 题目个数
        int correct=0;  //回答正确的题目个数
        int rate; //  题目等级
        double accuracy;  //  答题的正确率
        String []Question;  //存放题目的数组

        Scanner scanner=new Scanner(System.in);
        System.out.print("请您输入要生成题目的个数： ");
        num=scanner.nextInt();

        Question=new String[num];//实例化保存生成题目的数组

        System.out.print("请你输入生成题目的等级： ");
        rate=scanner.nextInt();

        Create create=new Create(num,rate);

        create.QuestionNum();
        Question=create.getArraylist();

        int start=0;
        while (start<num)
        {
            System.out.println("第"+(start+1)+"题： "+Question[start]+"\n"+"请输入您的计算结果： ");
            String answer=scanner.next();

            Transform trans=new Transform(Question[start]);
            trans.transform();

            String transformed =trans.getLast();

            Calculate calcu=new Calculate(transformed);
            calcu.ToResult();
            String result=calcu.getResult();

            if (answer.equals(result)==true)
            {
                correct++;
                System.out.println("正确答案是： "+result+" ,您的回答正确,请继续作答");
                System.out.println();
            }

            else
            {
                System.out.println("正确答案是： "+result+" ,您的回答错误,请继续作答");
                System.out.println();
            }

            start++;
        }

        System.out.println();

        accuracy=(double)correct/num;
        DecimalFormat fmt=new DecimalFormat("0.####");
        String accurancy2=fmt.format(accuracy);
        Double accurancy3=Double.valueOf(accurancy2);

        NumberFormat format = NumberFormat.getPercentInstance();

        System.out.println("您一共回答了"+num+"道题目，答对了"+correct+"道题目"+"\n答题的正确率为: "+format.format(accurancy3));
        System.out.println();
        if (accurancy3<0.6)
        {
            System.out.println("很遗憾，您的成绩没有及格，需要加把劲了！");
        }
        else{
            if (accurancy3<0.9)
                System.out.println("您的成绩不错，但是仍有进步空间，继续加油吧！");
            else
                System.out.println("您的成绩很好，希望您能继续努力，继续考出好成绩");
        }
    }
}
