package Calculator;

/*
 Fraction.java           作者：赵晓海  黄宇瑭
 用于生成随机最简分数或者整数且不大于50
 */
public class Fraction {
    int numerator, denominator;

    //构造函数
    public Fraction() {
        numerator= (int) (Math.random()*10);
        denominator= (int) (Math.random()*10);
        if(denominator==0)
            denominator=1;
        reduce();
    }
    //保证遵循分数的规则
    private void reduce() {
        if (numerator != 0) {
            int common = gcd(Math.abs(numerator), denominator);

            numerator = numerator / common;
            denominator = denominator / common;
        }
    }
    //保证遵循分数的规则
    private int gcd(int num1, int num2) {
        while (num1 != num2)
            if (num1 > num2)
                num1 = num1 - num2;
            else
                num2 = num2 - num1;

        return num1;
    }

    //得到最简分数或者整数的方法
    public String getFraction()
    {
        String result;
        if(numerator==0)
            result="0";
        else
        if(denominator==1)
            result=numerator+"";
        else
            result=numerator+"/"+denominator;
        return result;
    }
}

